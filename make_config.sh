#!/bin/bash
BASE_DIR=~/easyrsa/pki
OUTPUT_DIR=~/client-configs/files
BASE_CONFIG=~/client-configs/base.conf

cat ${BASE_CONFIG} \
    <(echo -e '<ca>') \
    ${BASE_DIR}/ca.crt \
    <(echo -e '</ca>\n<cert>') \
    ${BASE_DIR}/issued/${1}.crt \
    <(echo -e '</cert>\n<key>') \
    ${BASE_DIR}/private/${1}.key \
    <(echo -e '</key>\n<tls-auth>') \
    ${BASE_DIR}/ta.key \
    <(echo -e '</tls-auth>') \
    > ${OUTPUT_DIR}/${1}.development.ovpn;

